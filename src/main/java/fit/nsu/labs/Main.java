package fit.nsu.labs;

import fit.nsu.labs.components.CarBody;
import fit.nsu.labs.components.CarComponentFactory;
import fit.nsu.labs.components.CarEngine;
import fit.nsu.labs.storage.RamStorage;
import fit.nsu.labs.tasks.BuildCar;
import io.github.cdimascio.dotenv.Dotenv;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Main {
    public static void main(String[] args) {
        try{
            Dotenv dotenv = Dotenv.load();
            var carBodyFactory = new CarComponentFactory<>(
                    CarBody.class,
                    new RamStorage(Integer.parseInt(dotenv.get("STORAGE_BODY_SIZE")))
            );

            var carEngineFactory = new CarComponentFactory<>(
                    CarEngine.class,
                    new RamStorage(Integer.parseInt(dotenv.get("STORAGE_ENGINE_SIZE")))
            );

            var workers_count = Integer.parseInt(dotenv.get("WORKERS"));
            var executor = Executors.newFixedThreadPool(workers_count);
            ArrayList<Callable<Object>> callables = new ArrayList<>();
            for(int i = 0; i < workers_count; i++){
                Callable<Object> callable = Executors.callable(new BuildCar(carBodyFactory, carEngineFactory));
                callables.add(callable);
            }

            List<Future<Object>> answers = executor.invokeAll(callables);
            answers.stream().forEach(objectFuture -> {
                try {
                    objectFuture.get();
                } catch (InterruptedException | ExecutionException e) {
                    executor.shutdownNow();
                    throw new RuntimeException(e);
                }
            });
            executor.shutdownNow();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
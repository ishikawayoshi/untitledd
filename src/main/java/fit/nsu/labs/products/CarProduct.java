package fit.nsu.labs.products;

import fit.nsu.labs.components.CarBody;
import fit.nsu.labs.components.CarEngine;

public class CarProduct {
    private final CarBody body;
    private final CarEngine engine;

    public CarProduct(CarBody body, CarEngine engine) {
        this.body = body;
        this.engine = engine;
    }
}

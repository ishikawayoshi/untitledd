package fit.nsu.labs.tasks;

public interface Task extends Runnable {
    String getTaskName();
}
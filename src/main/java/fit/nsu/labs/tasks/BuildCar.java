package fit.nsu.labs.tasks;

import fit.nsu.labs.AtomicId;
import fit.nsu.labs.components.CarBody;
import fit.nsu.labs.components.CarComponentFactory;
import fit.nsu.labs.components.CarEngine;
import fit.nsu.labs.products.CarProduct;

public class BuildCar extends AtomicId implements Task{
    private final CarComponentFactory<CarBody> carBodyFactory;
    private final CarComponentFactory<CarEngine> carEngineFactory;
    public BuildCar(CarComponentFactory<CarBody> carBodyFactory, CarComponentFactory<CarEngine> carEngineFactory){
        this.carBodyFactory = carBodyFactory;
        this.carEngineFactory = carEngineFactory;
        nextId();
    }


    @Override
    public String getTaskName() {
        return "build car";
    }

    @Override
    public void run() {
        try {
            var body = carBodyFactory.produceElement();
            var engine = carEngineFactory.produceElement();
            var product = new CarProduct(body, engine);
            System.out.println(product.hashCode());
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}

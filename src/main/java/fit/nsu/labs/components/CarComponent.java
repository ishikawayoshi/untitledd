package fit.nsu.labs.components;

import fit.nsu.labs.AtomicId;

import java.util.concurrent.atomic.AtomicLong;

public abstract class CarComponent extends AtomicId {
    CarComponent(){
        nextId();
    }
}
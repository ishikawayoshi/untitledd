package fit.nsu.labs.components;

import fit.nsu.labs.storage.IStorage;
import fit.nsu.labs.storage.RamStorage;

import java.lang.reflect.InvocationTargetException;
import java.util.UUID;

public class CarComponentFactory<T extends CarComponent>{
    private final Class<T> componentClass;

    public RamStorage getStorage() {
        return storage;
    }

    private final RamStorage storage;

    private T createComponent(){
        try {
            return (T) componentClass.getConstructor().newInstance();
        } catch (InvocationTargetException | NoSuchMethodException | InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public CarComponentFactory(Class<T> componentClass, RamStorage storage){
        this.componentClass = componentClass;
        this.storage = storage;
    }

    public T produceElement() throws InterruptedException {
        var el = createComponent();
        storage.put(el);
        return el;
    }
}